# management-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Plugins In-Use
1. Vuex  [docs](https://vuex.vuejs.org/)
2. VueRouter [docs](https://router.vuejs.org/)
3. Vuetify [docs](https://vuetifyjs.com/en/getting-started/quick-start/)
4. axios [docs](https://github.com/axios/axios)
    
This application requires the startup of both the **Keycloak container** and the **order service**
