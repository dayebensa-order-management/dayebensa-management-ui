import HelloWorld from './components/HelloWorld'
import Orders from './views/OrderMain'
import OrderDetails from './components/orders/OrderDetails'
import Home from './components/Home'
import OrderList from './components/orders/OrdersList'
import ShipmentView from "./views/ShipmentView";
import OidcCallback from "./components/OidcCallback";
import VueRouter from "vue-router";
import Vue from 'vue';
import store from '@/store'
import {vuexOidcCreateRouterMiddleware} from 'vuex-oidc'
import ContractsView from "@/views/ContractsView";
import ContractView from "@/views/ContractView";
import OrderView from "@/views/OrderView";


Vue.use(VueRouter);

const routeNames = {
    toHome: 'homeRoute',
    toHello: 'helloRoute',
    toOrder: 'orderRoute',
    toOrderList: 'orderListRoute',
    toOrderDetails: 'orderDetailsRoute',
    toShipmentDetails: 'shipmentDetailsRoute',
    toCallback: 'oidcCallback',
    toContracts: 'contractsRoute',
    toContract: 'singleContractRoute',
    toSingleOrder: 'singleOrderRoute'
};

const routes = [
    {
        path: '', component: Home, name: routeNames.toHome, meta: {
            isPublic: true
        }
    },
    {path: '/hello', component: HelloWorld, name: routeNames.toHello},
    {
        path: '/order', component: Orders, children: [
            {
                path: '',
                component: OrderList,
                name: routeNames.toOrderList
            },
            {
                path: ':orderId',
                component: OrderDetails,
                name: routeNames.toOrderDetails,
                props: true
            },
            {
                path: ':orderId/shipment/:shipmentId',
                component: ShipmentView,
                name: routeNames.toShipmentDetails,
                props: true
            }

        ]
    },
    {
        path: '/oidc-callback', // Needs to match redirectUri (redirect_uri if you use snake case) in you oidcSettings
        name: routeNames.toCallback,
        component: OidcCallback
    },
    {
        path: '/contracts',
        name: routeNames.toContracts,
        component: ContractsView
    },
    {
        path: '/contracts/:contractId',
        name: routeNames.toContract,
        component: ContractView,
        props: true

    },
    {
        path: '/contracts/:contractId/orders/:orderId',
        name: routeNames.toSingleOrder,
        component: OrderView,
        props: true
    }
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach(vuexOidcCreateRouterMiddleware(store, 'oidcStore'));

export {routeNames, router};
