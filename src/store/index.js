import Vuex from 'vuex'
import Vue from 'vue'
import { vuexOidcCreateStoreModule } from 'vuex-oidc';
import {oidcSettings} from "@/config/oidc";
import * as order from "./modules/order";
import * as contract from "./modules/contract"
import OrderService from "../services/OrderService";


Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        contract,
        order,
        oidcStore: vuexOidcCreateStoreModule(
            oidcSettings,

            {
                namespaced: true,
                dispatchEventsOnWindow: true
            },
            // Optional OIDC event listeners
            {
                userLoaded: (user) => {
                    //Axios defaults
                    OrderService.initialize(user.access_token)
                    Vue.$log.info("Axios defaults setup successfully");
                },
                userUnloaded: () => Vue.$log.info('OIDC user is unloaded'),
                accessTokenExpiring: () => Vue.$log.info('Access token will expire'),
                accessTokenExpired: () => Vue.$log.info('Access token did expire'),
                silentRenewError: () => Vue.$log.info('OIDC user is unloaded'),
                userSignedOut: () => Vue.$log.info('OIDC user is signed out'),
                oidcError: (payload) => Vue.$log.info('OIDC error', payload)
            }
        )
    }
});
