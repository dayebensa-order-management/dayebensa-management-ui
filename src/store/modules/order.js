import OrderService from "../../services/OrderService";

export const namespaced = true

export const state = () => ({
    orders: [],
    products: [],
    shipments: [],
    order: {},
    product: {},
    shipment: {},
    productEvents: []
});



//getters
export const getters = {
    getOrderById: (state) => (id) => {
        return state.orders.find(order => order.id === id)
    },
    getProductById: (state) => (id) => {
        return state.products.find(product => product.id === id)
    },
    getShipmentById: (state) => (id) => {
        return state.shipments.find(shipment => shipment.id === id)
    },
    getProductsNotInShipment: (state) => (id) => {
        return state.products.find(product => product.shipmentId === id)
    }
};


//mutators
export const mutations = {
    SET_ORDERS(state,orders){
        state.orders = orders
    },
    SET_PRODUCTS(state,products){
        state.products = products
    },
    SET_SHIPMENTS(state,shipments){
        state.shipments = shipments
    },
    SET_ORDER(state,order){
        state.order = order
    },
    SET_PRODUCT(state,product){
        state.product = product
    },
    SET_SHIPMENT(state,shipment){
        state.shipment = shipment
    },
    SET_PRODUCT_EVENTS(state,events){
        state.productEvents = events
    }
}

export const actions = {
    fetchOrders({commit}){
        OrderService.getOrders()
            .then(response => {
                commit('SET_ORDERS',response.data)
            })
            .catch(err => {
                this.$log.error(err)
            })
    },
    fetchProducts({commit},orderId){
        OrderService.getProducts(orderId)
            .then(response => {
                commit('SET_PRODUCTS',response.data)
            })
            .catch(err => {
                this.$log.error(err)
            })
    },
    fetchProductsByShipment({commit},{orderId,shipmentId}){

        OrderService.getProductsByShipment(orderId,shipmentId)
            .then(response => {
                commit('SET_PRODUCTS',response.data)
            })
            .catch(err => {
                this.$log.error(err)
            })

    },
    fetchShipments({commit},orderId){
        OrderService.getShipments(orderId)
            .then(response => {
                commit('SET_SHIPMENTS',response.data)
            })
            .catch(err => {
                this.$log.error(err)
            })
    },
    fetchOrder({commit,getters},orderId){
        var order = getters.getOrderById(orderId)

        if(order){
            commit('SET_ORDER',order)
        } else {
            OrderService.getOrder(orderId)
                .then(response => {
                    commit('SET_ORDER', response.data)
                })
                .catch(err => {
                    this.$log.error(err)
                })
        }
    },
    fetchProduct({commit,getters},{orderId,productId}){

        var product = getters.getProdutById(productId)

        if(product){
            commit('SET_PRODUCT',product)
        } else {
            OrderService.getProduct(orderId, productId)
                .then(response => {
                    commit('SET_PRODUCT', response.data)
                })
                .catch(err => {
                    this.$log.error(err)
                })
        }
    },
    fetchShipment({commit,getters},{orderId,shipmentId}){

        var shipment = getters.getShipmentById(shipmentId)

        if(shipment){
            commit('SET_SHIPMENT',shipment)
        } else {
            OrderService.getShipment(orderId, shipmentId)
                .then(response => {
                    commit('SET_SHIPMENT', response.data)
                })
                .catch(err => {
                    this.$log.error(err)
                })
        }
    },

    fetchProductEvents({commit},{orderId,productId}){
        OrderService.getProductEvents(orderId,productId)
            .then( response => {
                commit('SET_PRODUCT_EVENTS',response.data)
            })
            .catch( err => {
                this.$log.error(err)
            })
     }

    // addProductToShipment({commit},{orderId, shipmentId, product}) {
    //     OrderService.addProductToShipment(orderId,shipmentId,product)
    //         .catch( err => {
    //             this.$log.error(err)
    //         })
    // }

}



