import OrderService from "@/services/OrderService";
import Vue from "vue";


export const namespaced = true

export const state = () => ({
    contracts: [],
    orders: [],
    contract: {},
    order: {}
})


export const getters = {

    getContract: state => {
        return state.contract
    }
}

export const mutations = {
    SET_CONTRACTS(state, contracts) {
        state.contracts = contracts
    },
    ADD_CONTRACT(state, contract) {
        state.contracts.push(contract)
    },
    SET_ORDERS(state, orders) {
        state.orders = orders
    },
    SET_CONTRACT(state, contract) {
        state.contract = contract
    },
    ADD_ORDER(state, order) {
        state.orders.push(order)
    },
    SET_ORDER(state, order) {
        state.order = order
    }
}

export const actions = {
    fetchContracts({commit}) {
        OrderService.getContracts()
            .then(response => {
                commit('SET_CONTRACTS', response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    fetchContract({commit}, contractId) {
        OrderService.getContract(contractId)
            .then(response => {
                commit('SET_CONTRACT', response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    createContract({commit}, contract) {

        OrderService.createContract(contract)
            .then(response => {
                commit('ADD_CONTRACT', response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    fetchOrders({commit}, contractId) {
        OrderService.getOrders(contractId)
            .then(response => {
                commit('SET_ORDERS', response.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    // eslint-disable-next-line no-unused-vars
    createOrder({commit}, {order, contractId}) {
        OrderService.createOrder(order, contractId)
            .then(response => {
                Vue.$log.info(response)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },
    fetchOrder({commit}, {contractId, orderId}) {
        OrderService.getOrderById(contractId, orderId)
            .then(resp => {
                commit('SET_ORDER', resp.data)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    // eslint-disable-next-line no-unused-vars
    deleteOrder({commit}, {contractId, orderId}) {
        OrderService.deleteOrderById(contractId, orderId)
            .then(resp => {
                Vue.$log.info(resp)
            })
            .catch(err => {
                Vue.$log.error(err)
            })
    },

    // eslint-disable-next-line no-unused-vars
    deleteContract({commit}, contractId) {
        OrderService.deleteContract(contractId)
            .then(resp => {
                Vue.$log.info(resp)
            })
            .catch(err => {
                Vue.$log.error(err)
            })

    }
}
