import axios from 'axios'

const apiClient = axios.create({
    baseURL: 'https://52zg2d4363.execute-api.us-east-1.amazonaws.com/dev'
});


export default {
    initialize(bearerToken){
        apiClient.defaults.headers.common['Authorization'] = 'Bearer ' + bearerToken
    },

    getContracts(){
      return apiClient.get('/contracts')
    },

    getContract(contractId){
        return apiClient.get('/contracts/' +contractId)
    },

    createContract(contract){
      return apiClient.post('/contracts/',contract)
    },
    getOrders(contractId){
        return apiClient.get('/contracts/' +contractId +'/orders')
    },

    createOrder(order,contractId){
      return apiClient.post('/contracts/' + contractId + '/orders', order)
    },

    getOrderById(contractId, orderId){
        return apiClient.get('/contracts/' + contractId + '/orders/' + orderId)
    },

    deleteOrderById(contractId, orderId){
        return apiClient.delete('/contracts/' + contractId + '/orders/' + orderId)
    },

    deleteContract(contractId){
        return apiClient.delete('/contracts/' + contractId)
    },


    //Legacy functions to be removed
    getOrder(id){
        return apiClient.get('/orders/' + id)
    },

    getShipments(orderId){
      return  apiClient.get('/orders/' + orderId + '/shipments')
    },
    getShipment(orderId,shipmentId) {
        return apiClient.get('/orders/' + orderId +'/shipments/' + shipmentId)
    },

    getProducts(orderId){
        return apiClient.get('/orders/' + orderId + '/products')
    },

    getProduct(orderId,productId){
        return apiClient.get('/orders/' + orderId + '/products/' + productId)
    },

    getProductsByShipment(orderId,shipmentId){
        return apiClient.get('/orders/' + orderId + '/shipments/' + shipmentId
            + '/products')
    },
    getProductEvents(orderId,productId){
        return apiClient.get('/orders/' + orderId + '/products/' +productId +'/events')
    },

    addProductToShipment(orderId, shipmentId, product){
      return apiClient.put('/orders/' +orderId + '/shipments/' + shipmentId + '/products/',
            product)
    }

    //TODO: Add missing api calls


}
