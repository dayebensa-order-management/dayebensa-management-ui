export const oidcSettings = {
    authority: 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_SMUKbYm1l/.well-known/openid-configuration',
    clientId: '7mfgevi4i9e9asllk7polebunq',
    redirectUri: window.location.origin + '/oidc-callback',
    responseType: 'code',
    scope: 'openid profile phone email',
    silentRedirectUri: window.location.origin + '/oidc-silent-renew.html',
    automaticSilentRenew: true
}