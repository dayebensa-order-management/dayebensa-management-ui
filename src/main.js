import Vue from 'vue'
import App from './App.vue'
import VueLogger from 'vuejs-logger';
import {router} from './routes.js'
import store from './store'
import vuetify from './plugins/vuetify';


Vue.config.productionTip = false;

const options = {
    isEnabled: true,
    logLevel: Vue.config.productionTip ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
};


Vue.use(VueLogger, options);


new Vue({
    render: h => h(App),
    store,
    vuetify,
    router
}).$mount('#app');

